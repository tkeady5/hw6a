CC = g++
CXXFLAGS = -std=c++11 -pedantic -Wall -Wextra -O -g
GPROF = -pg
GCOV = -fprofile-arcs -ftest-coverage

bin: hw6
#	$(CC) $(CXXFLAGS) CTree.o

hw6: CTree.o

CTree.o: CTree.h CTree.cpp


clean:
	rm -rf *.o CTree

