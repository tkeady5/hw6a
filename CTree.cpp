#include "CTree.h"
#include <string>




/*
 * Basic constructor puts the character as the data of this node
 */
//CTree::CTree(char ch) : data(ch) {}

/*
 * Real constructor has ability to add additional information about this node
 */
CTree::CTree(char ch, CTree* k, CTree* s, CTree* p) : data(ch), kids(k), sibs(s), prev(p) {}


/*
 * Desctructor deletes the last child of the last sibling, the 
 * second to last child and so on until it deletes the sibling 
 * itself, then moves on to the second to last sibling and repeats
 * until it reaches the root node
 */
CTree::~CTree() {
	printf("Removing\n");
}








// Want two versions
// 	this one takes hte char and puts it in the right place
// but the other version is given the root of an existing tree,
// and the one that takes a char can just make the node then call
// this one 
// so below code is unnecessary :(
bool CTree::addSibling(char ch) {
	if ( (sibs == NULL) || (sibs->data < ch) ) {
		sibs = &CTree(ch);		// is this allowed?
	} else if (sibs->data > ch) {
		return addSibling(ch);
	} else if (sibs->data == ch) {
		return false; 		// If the chars match
	}
}



